package game

// GameState holds all game data

type GameState struct {
	Players  []Player   `json:"players"`   // holds players, which are shown on screen
	Map      []Tile     `json:"map"`       // holds map, which is shown on screen
	Bombs    []Bomb     `json:"bombs"`     // holds bombs, which are shown on screen
	PowerUps []*PowerUp `json:"power_ups"` // holds power ups, which are shown on screen
	State    State      `json:"state"`     // holds current game state
}

// NewGame creates new game state
func NewGame() *GameState {
	return &GameState{
		Players:  make([]Player, 0),
		Bombs:    make([]Bomb, 0),
		Map:      make([]Tile, 0),
		PowerUps: make([]*PowerUp, 0),
		State:    Lobby,
	}
}

// StartGame creates new map and sets game state to Play
func (g *GameState) StartGame() {
	g.Map = CreateBaseMap()
	g.State = Play
}

// Clear clears all game data
func (g *GameState) Clear() {
	g.Map = []Tile{}
	g.Bombs = []Bomb{}
	g.PowerUps = []*PowerUp{}
	g.State = Lobby
}

// FindPlayer finds player by name and returns index of player in game state slice
func (g *GameState) FindPlayer(name string) int {
	for index, player := range g.Players {
		if player.Name == name {
			return index
		}
	}

	return -1
}

// IsPlayer checks if player is in game state slice
func (g *GameState) IsPlayer(name string) bool {
	playerIndex := g.FindPlayer(name)
	return playerIndex != -1
}

// check how many players are still alive.
// in case of 1 player left -> game over
func (g *GameState) CheckGameOverState() bool {
	playersAlive := 0
	for _, player := range g.Players {
		if player.Movement != Died {
			playersAlive += 1
		}
	}
	if playersAlive == 1 && len(g.Players) != 1 {
		g.State = GameOver
		return true
	}

	return false
}

// check how many players are still alive. If only one player left -> clear game
func (g *GameState) ClearGameIfLastPlayerLeft() bool {
	playersAlive := 0
	for _, player := range g.Players {
		if player.Movement != Died {
			playersAlive += 1
		}
	}
	if playersAlive == 1 {
		g.Clear()
		return true
	}

	return false

}

// Loop through all players in game and check if somebody is in the explosion
// return slice with monster that died
func (g *GameState) CheckIfSomebodyDied(explosion *Explosion) []int {
	var monstersLostLives = []int{}
	if g.State != Play {
		return monstersLostLives
	}
	for i := 0; i < len(g.Players); i++ {
		var lostLive = g.Players[i].CheckIfIDie(explosion)
		if lostLive {
			monstersLostLives = append(monstersLostLives, i)
		}
	}
	g.CheckGameOverState()
	return monstersLostLives
}

// Loop through all active explosion in game and check if current player stepped in it
func (g *GameState) CheckIfPlayerDied(p *Player) bool {
	var lostLive = false
	if g.State != Play {
		return lostLive
	}
	for _, player := range g.Players {
		for _, explosion := range player.Explosions {
			lostLive = p.CheckIfIDie(&explosion)
			if lostLive {
				return lostLive
			}
		}
	}
	g.CheckGameOverState()
	return lostLive
}

// check if destroyed block index match with powerup block index
func (g *GameState) RevealPowerUps(destroyedBlocks []int) {
	for _, blockIndex := range destroyedBlocks {
		for _, powerUp := range GeneratedPowerUps {
			if blockIndex == powerUp.Tile {
				g.PowerUps = append(g.PowerUps, powerUp)
			}
		}
	}
}

// let monsters reborn after 5 seconds
func (g *GameState) LetMonstersReborn(monstersLostLives []int) {
	for _, i := range monstersLostLives { //reset the movement
		g.Players[i].Invincible = false
	}
}

// check if player stepped on powerup and if so, apply powerup effect to player and remove powerup from game
func (g *GameState) FindWinner() string {
	for _, player := range g.Players {
		if player.Movement != Died {
			return player.Name
		}
	}
	return ""
}

// is there a bomb on the tile with index tileIndex ? If so, return true
func (g *GameState) IsThereBomb(tileIndex int) bool {
	for _, player := range g.Players {
		for _, bomb := range player.Bombs {
			if bomb.Tile == tileIndex {
				return true
			}
		}
	}
	return false
}

// map is empty if there are no tiles in map
func (g *GameState) MapIsEmpty() bool {
	return len(g.Map) == 0
}

// TurnTileIntoWall turns tile with index into wall tile type
func (g *GameState) TurnTileIntoWall(index int) {
	g.Map[index] = Wall
}

// RemovePowerupInPlace removes powerup from game state slice
func (g *GameState) RemovePowerupInPlace(index int) {
	for puIndex, powerUp := range g.PowerUps {
		if powerUp.Tile == index {
			g.PowerUps = append(g.PowerUps[:puIndex], g.PowerUps[puIndex+1:]...)
		}
	}
}
