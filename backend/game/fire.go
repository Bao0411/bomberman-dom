package game

import "math"

type Fire struct {
	X    int `json:"x"`
	Y    int `json:"y"`
	Type int `json:"type"`
}

// IsMonsterInside checks if monster is inside fire range
func (fire *Fire) IsMonsterInside(x, y float64) bool {
	var errorMargin = 5 // 5px

	var monsterLeft = int(math.Round(x)) + errorMargin // round to int and add error margin to make monster detection range smaller
	var monsterRight = int(math.Round(x)) + 64 - errorMargin
	var monsterUp = int(math.Round(y)) + errorMargin
	var monsterDown = int(math.Round(y)) + 64 - errorMargin

	// check if monster is in fire range (64px) and return true if it is in range
	if monsterLeft >= fire.X && monsterLeft <= fire.X+64 || monsterRight >= fire.X && monsterRight <= fire.X+64 {
		if monsterUp >= fire.Y && monsterUp <= fire.Y+64 || monsterDown >= fire.Y && monsterDown <= fire.Y+64 {
			return true
		}
	}
	return false
}
