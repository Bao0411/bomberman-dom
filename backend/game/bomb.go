package game

// Bomb is a struct for bombs
// X and Y are coordinates
// Tile is a tile number
// Name is a name of the bomb
// Movement is a movement of the bomb
// Speed is a speed of the bomb

type Bomb struct {
	X        int      `json:"x"`
	Y        int      `json:"y"`
	Tile     int      `json:"-"`
	Name     string   `json:"name"`
	Movement Movement `json:"movement"`
	Speed    int      `json:"-"` //for changing how fast is movement
}
