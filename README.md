

### Starting the project

1. Install necessary dependencies `npm install`
2. Start frontend server `npm run dev`
3. Run backend server by going to `/backend` folder and running `go run server.go`




