## collision

- https://www.youtube.com/watch?v=r0sy-Cr6WHY&t=327s

## mini-vue

- https://dev.to/themarcba/coding-a-vue-js-like-framework-from-scratch-part-1-introduction-3nbf
- https://medium.com/@lachlanmiller_52885/building-vuex-from-scratch-9ac47c768f9d

## parcel

- https://www.digitalocean.com/community/tutorials/how-to-bundle-a-web-app-with-parcel-js
- https://parceljs.org/#:~:text=Parcel%20can%20build%20libraries%20for,takes%20care%20of%20the%20rest.
